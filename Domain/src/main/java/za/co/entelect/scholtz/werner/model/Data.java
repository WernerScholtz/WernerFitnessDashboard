package za.co.entelect.scholtz.werner.model;


public class Data {

    private double exerciseTime;
    private int averageHeartRate;

    public double getExerciseTime() {
        return exerciseTime;
    }

    public int getAverageHeartRate() {
        return averageHeartRate;
    }

    public void setExerciseTime(double exerciseTime) {
        this.exerciseTime = exerciseTime;
    }

    public void setAverageHeartRate(int averageHeartRate) {
        this.averageHeartRate = averageHeartRate;
    }

}
