package za.co.entelect.scholtz.werner.model;

public class User {

    public enum validGenders{male, female, apacheattackhelicopter};

    private String name;
    private String surname;
    private validGenders gender;
    private int age;
    private int weight;
    private double height;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public validGenders getGender() {
        return gender;
    }

    public void setGender(validGenders gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

}
