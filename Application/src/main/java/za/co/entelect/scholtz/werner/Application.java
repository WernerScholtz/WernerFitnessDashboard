package za.co.entelect.scholtz.werner;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import za.co.entelect.scholtz.werner.config.ApplicationConfig;
import za.co.entelect.scholtz.werner.model.Data;
import za.co.entelect.scholtz.werner.model.User;
import za.co.entelect.scholtz.werner.service.CalculatorService;
import za.co.entelect.scholtz.werner.service.DataService;
import za.co.entelect.scholtz.werner.service.UserService;

public class Application {

    public static void main(String[] args) {

        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(ApplicationConfig.class);

        UserService userService = applicationContext.getBean("userService", UserService.class);
        CalculatorService calc = applicationContext.getBean("calculatorService", CalculatorService.class);
        DataService dataService = applicationContext.getBean("dataService", DataService.class);

        User user = userService.findAll().get(0);
        Data data = dataService.findAllData().get(0);

        System.out.println(" ");

        System.out.println(user.getName() + " " + user.getSurname() + " is a " + user.getAge() + " year old " + user.getGender() + ", weighs " + user.getWeight() + "kg with a height of " + user.getHeight() + "m.");
        System.out.printf("%s burned %.2f calories with an average heart rate of %d bpm over %.2f hours.%n", user.getName(), calc.getCaloriesBurnt(user,data.getAverageHeartRate(),data.getExerciseTime()), data.getAverageHeartRate(), data.getExerciseTime());

    }
}
