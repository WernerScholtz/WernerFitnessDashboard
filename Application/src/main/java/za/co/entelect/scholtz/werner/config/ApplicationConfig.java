package za.co.entelect.scholtz.werner.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
//import za.co.entelect.scholtz.werner.repository.UserRepository;
//import za.co.entelect.scholtz.werner.repository.UserRepositoryImpl;
//import za.co.entelect.scholtz.werner.service.UserService;
//import za.co.entelect.scholtz.werner.service.UserServiceImpl;


@Configuration
@ComponentScan({"za.co.entelect.scholtz.werner"})
public class ApplicationConfig {

//    @Bean(name = "userService")
//    public UserService getCustomerService() {
//        UserServiceImpl service = new UserServiceImpl(getUserRepository()); // Constructor Injection
////        UserServiceImpl service = new UserServiceImpl(); // Setter Injection
////        service.setUserRepository(getCustomerRepository()); // Setter Injection
//        return service;
//    }
//
//    @Bean(name = "userRepository")
//    public UserRepository getUserRepository(){
//        return new UserRepositoryImpl();
//    }

}
