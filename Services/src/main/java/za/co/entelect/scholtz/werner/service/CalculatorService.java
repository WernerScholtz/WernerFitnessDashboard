package za.co.entelect.scholtz.werner.service;

import za.co.entelect.scholtz.werner.model.User;

public interface CalculatorService {
    double getCaloriesBurnt(User user, int averageHeartRate, double exerciseTime);
}
