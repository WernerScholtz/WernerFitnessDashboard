package za.co.entelect.scholtz.werner.service;

import za.co.entelect.scholtz.werner.model.Data;

import java.util.List;

public interface DataService {
    List<Data> findAllData();
}
