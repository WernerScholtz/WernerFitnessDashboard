package za.co.entelect.scholtz.werner.service;

import za.co.entelect.scholtz.werner.model.User;
import java.util.List;

public interface UserService {
    List<User> findAll();
}
