package za.co.entelect.scholtz.werner.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import za.co.entelect.scholtz.werner.model.Data;
import za.co.entelect.scholtz.werner.repository.DataRepository;

import java.util.List;

@Service("dataService")
@Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
public class DataServiceImpl implements DataService{

    private DataRepository dataRepository;

    @Autowired
    public DataServiceImpl(DataRepository dataRepository) { // Constructor injection
        this.dataRepository = dataRepository;
    }

    public List<Data> findAllData(){
        return dataRepository.findAllData();
    }

}
