package za.co.entelect.scholtz.werner.service;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import za.co.entelect.scholtz.werner.model.User;

@Service("calculatorService")
@Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
public class CalculatorServiceImpl implements CalculatorService {

    private double calculateCaloriesBurnt(User user, int averageHeartRate, double exerciseTime) {
        double caloriesBurnt;
        switch (user.getGender()) {
            case male:
                caloriesBurnt = ((-55.0969 + (0.6309 * averageHeartRate) + (0.1988 * user.getWeight()) + (0.2017 * user.getAge())) / 4.184) * 60 * exerciseTime;
                break;

            case female:
                caloriesBurnt = ((-20.4022 + (0.4472 * averageHeartRate) + (0.1263 * user.getWeight()) + (0.074 * user.getAge())) / 4.184) * 60 * exerciseTime;
                break;
            case apacheattackhelicopter:
                caloriesBurnt = ((-55.0969 + (0.6309 * averageHeartRate) + (0.1988 * user.getWeight()) + (0.2017 * user.getAge())) / 4.184) * 60 * exerciseTime*9999999999.9;
                break;
            default:
                caloriesBurnt = 0;
        }
        return caloriesBurnt;
    }

    @Override
    public double getCaloriesBurnt(User user, int averageHeartRate, double exerciseTime) {
        return calculateCaloriesBurnt(user, averageHeartRate, exerciseTime);
    }

}
