package za.co.entelect.scholtz.werner.repository;

import za.co.entelect.scholtz.werner.model.User;
import java.util.List;

public interface UserRepository {
    List<User> findAll();
}
