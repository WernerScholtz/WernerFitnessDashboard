package za.co.entelect.scholtz.werner.repository;

import org.springframework.stereotype.Repository;
import za.co.entelect.scholtz.werner.model.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

@Repository("dataRepository")
public class DataRepositoryImpl implements DataRepository {

    private Scanner scanner = new Scanner(System.in);
    private Data data = new Data();
    private List<Data> dataList = new ArrayList<>();

    @Override
    public List<Data> findAllData(){

        System.out.println("Average heart rate during exercise [bpm]:");
        data.setAverageHeartRate(scanner.nextInt());
        System.out.println("Exercise time [hours]:");
        data.setExerciseTime(scanner.nextDouble());

        dataList.add(data);

        return dataList;
    }

}
