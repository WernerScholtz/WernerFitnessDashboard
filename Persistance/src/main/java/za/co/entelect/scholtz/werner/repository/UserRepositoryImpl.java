package za.co.entelect.scholtz.werner.repository;

import org.springframework.stereotype.Repository;
import za.co.entelect.scholtz.werner.model.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

@Repository("userRepository")
public class UserRepositoryImpl implements UserRepository {

    private Scanner scanner = new Scanner(System.in);
    private User user = new User();
    private List<User> users = new ArrayList<>();
    private boolean inValidGender = true;

    @Override
    public List<User> findAll() {

//        user.setName("Werner");
//        user.setSurname("Scholtz");
//        user.setGender("Female");
//        user.setAge(23);
//        user.setWeight(70);
//        user.setHeight(1.7);

        System.out.println("User name:");
        user.setName(scanner.nextLine());
        System.out.println("User surname:");
        user.setSurname(scanner.nextLine());

        System.out.println("User gender [Male/Female]:");
        while (inValidGender) {
            try {
                user.setGender(User.validGenders.valueOf(scanner.nextLine().toLowerCase()));
                inValidGender = false;
            } catch (IllegalArgumentException e) {
                System.out.println("Unfortunately only Male and Female is accepted in this program (and Apacheattackhelicopter ;) ). Please enter your gender [Male/Female]:");
            }
        }

        System.out.println("User age [years]:");
        user.setAge(scanner.nextInt());
        System.out.println("User weight [kg]:");
        user.setWeight(scanner.nextInt());
        System.out.println("User height [m]:");
        user.setHeight(scanner.nextDouble());

        users.add(user);

        return users;

    }

}
