package za.co.entelect.scholtz.werner.repository;


import za.co.entelect.scholtz.werner.model.Data;

import java.util.List;

public interface DataRepository {
    List<Data> findAllData();
}
